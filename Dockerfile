FROM ubuntu:20.04

ENV DEBIAN_FRONTEND=noninteractive

RUN echo 'tzdata tzdata/Areas select Europe' | debconf-set-selections
RUN echo 'tzdata tzdata/Zones/Europe select Amsterdam' | debconf-set-selections

RUN apt update

RUN apt -y install texlive texlive-lang-german texlive-latex-extra texlive-xetex latexmk texlive-bibtex-extra biber

RUN echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections
RUN apt -y install ttf-mscorefonts-installer 
